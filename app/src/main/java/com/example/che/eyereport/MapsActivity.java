package com.example.che.eyereport;

import android.app.DialogFragment;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import java.security.Provider;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MapsActivity extends FragmentActivity implements LocationListener {
GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        googleMap = fm.getMap();
        googleMap.setMyLocationEnabled(true);

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        for (String provider : locationManager.getAllProviders()) {
            Log.d("provider", provider);
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            } else {
                locationManager.requestLocationUpdates(provider, 400, 1000, this);
                break;
            }
        }

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                // TODO Auto-generated method stub
                CreateReport createReportFragment = CreateReport.newInstance(point, googleMap);
                createReportFragment.show(getFragmentManager(), "create_report");
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
//        double latitude = 14.5537422;
//        double longitude = 121.0218926;
//
//        googleMap.setMyLocationEnabled(true);
//        LatLng latLng = new LatLng(latitude, longitude);
//
//        googleMap.addMarker(new MarkerOptions()
//                .title("You're Here")
//                .snippet("Tap on the place of crime to report!")
//                .position(latLng));
//
//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);

            googleMap.addMarker(new MarkerOptions()
                    .title("You're Here")
                    .snippet("Tap on the place of crime to report!")
                    .position(latLng)
                    .flat(true));

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

        List<Report> allReports = Report.listAll(Report.class);
        for (Report summary : allReports) {
            Log.d("sql", summary.getTitle());
            Log.d("sql", summary.getDescription());
            Log.d("sql", summary.getLatitude());
            Log.d("sql", summary.getLongitude());
            Log.d("sql", summary.getIncidentType());

            double new_latitude = Double.parseDouble(summary.getLatitude());
            double new_longitude = Double.parseDouble(summary.getLongitude());

            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(new_latitude, new_longitude))
                    .title(summary.getIncidentType()+": "+summary.getTitle())
                    .snippet("STATUS: "+summary.getStatus()+"\n"+summary.getDescription());

            googleMap.addMarker(marker);
        }

        try {
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://128.199.174.115:8815").build();
            Reports reports = restAdapter.create(Reports.class);
            reports.getUserReports("75566545", new Callback<List<Report2>>() {
                @Override
                public void success(List<Report2> reports, Response response) {
                    for (Report2 report : reports) {
                        double new_latitude = Double.parseDouble(report.getLatitude());
                        double new_longitude = Double.parseDouble(report.getLongitude());

                        MarkerOptions marker = new MarkerOptions().position(
                                new LatLng(new_latitude, new_longitude))
                                .title(report.getIncidentType() + ": " + report.getTitle())
                                .snippet("STATUS: " + report.getStatus() + "\n" + report.getDescription());

                        googleMap.addMarker(marker);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("retro", error.getMessage());
                }
            });

        } catch (Exception e) {
            Log.e("network", "no net or no list: "+e.getMessage());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
