package com.example.che.eyereport;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.RequestQueue;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.orm.SugarApp;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CreateReportInfo extends DialogFragment implements View.OnClickListener {

    EditText summary, info;
    LatLng location;
    String type;
    GoogleMap googleMap;

    @SuppressLint("ValidFragment")
    public CreateReportInfo(LatLng location, String type, GoogleMap googleMap) {
        this.location = location;
        this.type = type;
        this.googleMap = googleMap;
    }

    public static CreateReportInfo newInstance(LatLng location, String type, GoogleMap googleMap) {
        CreateReportInfo createReportInfo = new CreateReportInfo(location, type, googleMap);
        return createReportInfo;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_create_report_info, new LinearLayout(getActivity()), false);

        // Build dialog
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setContentView(view);
        summary = (EditText) view.findViewById(R.id.summary);
        info = (EditText) view.findViewById(R.id.info);
        Button submit = (Button) view.findViewById(R.id.submit);
        submit.setOnClickListener(this);
        return builder;
    }

    @Override
    public void onClick(View v) {
        String summary_text = summary.getEditableText().toString();
        String info_text = info.getEditableText().toString();
        Report report = new Report(this.type, summary_text, info_text, "75566545", String.valueOf(location.latitude), String.valueOf(location.longitude), "PENDING");
        report.save();
        List<Report> allReports = Report.listAll(Report.class);
        for (Report summary : allReports) {
//            Log.d("sql", summary.getSummary());
//            Log.d("sql", summary.getInfo());
//            Log.d("sql", summary.getLatitude());
//            Log.d("sql", summary.getLongitude());
//            Log.d("sql", summary.getLevel());
        }
        dismiss();

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://128.199.174.115:8815").build();
        Reports reports = restAdapter.create(Reports.class);

        reports.postReport(report, new Callback<Report>() {
            @Override
            public void success(Report report, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(location.latitude, location.longitude))
                .title(this.type + ": " + summary_text)
                .snippet("STATUS: " + "PENDING" + "\n" + info_text)
                .draggable(true);

        googleMap.addMarker(marker);
    }
}
