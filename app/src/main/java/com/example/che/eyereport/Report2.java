package com.example.che.eyereport;

import com.orm.SugarRecord;

/**
 * Created by Che on 8/9/2015.
 */
public class Report2 extends SugarRecord<Report2> {
    String incidentType, title, description, citizenId, latitude, longitude, status;
    String datecreated;

    public Report2() {}

    public Report2(String datecreated, String incidentType, String title, String description, String citizenId, String latitude, String longitude, String status) {
        this.incidentType = incidentType;
        this.title = title;
        this.description = description;
        this.citizenId = citizenId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.datecreated = datecreated;
    }

    public String getIncidentType() {
        return incidentType;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getStatus() {
        return status;
    }
}
