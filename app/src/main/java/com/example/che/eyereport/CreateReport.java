package com.example.che.eyereport;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class CreateReport extends DialogFragment implements View.OnClickListener {
    LatLng location;
    GoogleMap googleMap;

    public CreateReport() {}

    @SuppressLint("ValidFragment")
    public CreateReport(LatLng location, GoogleMap googleMap) {
        this.location = location;
        this.googleMap = googleMap;
    }

    public static CreateReport newInstance(LatLng location, GoogleMap googleMap) {
        CreateReport createReport = new CreateReport(location, googleMap);
        return createReport;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_create_report, new LinearLayout(getActivity()), false);

        // Build dialog
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setContentView(view);
        Button organized = (Button) view.findViewById(R.id.organized);
        organized.setOnClickListener(this);
        Button petty = (Button) view.findViewById(R.id.petty);
        petty.setOnClickListener(this);
        Button violent = (Button) view.findViewById(R.id.violent);
        violent.setOnClickListener(this);
        Button corruption = (Button) view.findViewById(R.id.corruption);
        corruption.setOnClickListener(this);
        return builder;
    }

    @Override
    public void onClick(View v) {
        Button crime_button = (Button) v;
        String crimeLevel = crime_button.getText().toString();
        Log.d("click",crimeLevel);
        CreateReportInfo createReportInfo = CreateReportInfo.newInstance(location, crimeLevel, googleMap);
        createReportInfo.show(getFragmentManager(), "additional info");
        dismiss();
    }
}
