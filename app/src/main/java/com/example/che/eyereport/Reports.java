package com.example.che.eyereport;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Che on 8/9/2015.
 */
public interface Reports {

    @POST("/api/report")
    void postReport(@Body Report report, Callback<Report> cb);

    @GET("/api/reports/citizen/{id}")
    void getUserReports(@Path("id") String id, Callback<List<Report2>> cb);
}
